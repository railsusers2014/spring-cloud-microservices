package com.cloud;

import com.cloud.dao.TeamDao;
import com.cloud.domain.Player;
import com.cloud.domain.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class SpringCloudMicroservicesApplication extends SpringBootServletInitializer {

    /**
     * Used when run as a JAR
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudMicroservicesApplication.class, args);
    }

    /**
     * Used run as a WAR
     *
     * @param builder
     * @return
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(SpringCloudMicroservicesApplication.class);
    }

    @PostConstruct
    public void init() {
        Set<Player> players = new HashSet<>();
        players.add(new Player("Charlie Brown", "pitcher"));
        players.add(new Player("Snoopy", "shortstop"));

        Team team = new Team("California", "Peanuts", players);
        teamDao.save(team);
    }

    @Autowired
    TeamDao teamDao;
}
