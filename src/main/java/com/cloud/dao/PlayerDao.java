package com.cloud.dao;

import com.cloud.domain.Player;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by szagoret on 31.05.2016.
 */

/**
 * Spring will automatically create Controller and request mapping for this
 */
@RestResource(path = "players", rel = "players")
public interface PlayerDao extends CrudRepository<Player, Long> {
}
