package com.cloud.dao;

import com.cloud.domain.Team;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

/**
 * Created by szagoret on 31.05.2016.
 */
@RestResource(path = "teams", rel = "teams")
public interface TeamDao extends CrudRepository<Team, Long> {
    List<Team> findAll();

    Team findByName(String name);
}
