package com.cloud.web;

import com.cloud.dao.TeamDao;
import com.cloud.domain.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by szagoret on 31.05.2016.
 */
@RestController
public class WhateverIWant {


    @Autowired
    TeamDao teamDao;

    /**
     * You could call this function with:
     * http://localhost:8080/teams/Peanuts.json
     * http://localhost:8080/teams/Peanuts.xml
     * and returned type will be automatically converted to json or xml
     *
     * @return
     */
    @RequestMapping("/hi/{team}")
    public @ResponseBody Team hiThere(@PathVariable String team) {
        return teamDao.findByName(team);
    }
}
